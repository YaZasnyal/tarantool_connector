#pragma once

#include <iostream> // TODO: delete
#include <memory>

#include <boost/asio.hpp>
#include <boost/asio/spawn.hpp>

#include "network_interface.h"

namespace tarantool_connector
{

class BasicNetwork : public NetworkInterface
{
public:
    /**
     * @brief BasicNetwork
     * @param ctx
     */
    BasicNetwork(boost::asio::io_context& ctx) :
        ctx_(ctx),
        socket_(ctx_)
    {
    }

    ~BasicNetwork();

    // NetworkInterface interface
    void ConnectAsync(const std::string& host, uint16_t port,
                      std::function<void (const boost::system::error_code &)> &&handler);
    bool SendData(const DataType &data);
    bool SendData(DataType &&data);

private:
    bool SendData(std::unique_ptr<std::string>&& buf);
    boost::asio::io_context& ctx_;
    boost::asio::ip::tcp::socket socket_;
};

BasicNetwork::~BasicNetwork()
{
    socket_.close();
}

void BasicNetwork::ConnectAsync(const std::string& host, uint16_t port,
                                std::function<void (const boost::system::error_code &)> &&handler)
{
    boost::asio::spawn(ctx_, [this, &host, port, handler = std::move(handler)](boost::asio::yield_context yield) mutable
    {
        boost::asio::ip::tcp::resolver resolver(ctx_);
        boost::asio::ip::tcp::resolver::query query(host, std::to_string(port));
        boost::system::error_code ec;

        auto resolveResult = resolver.async_resolve(query, yield[ec]);
        if (ec) {
            handler(make_error_code(Errors::HostResolveError));
            return;
        }

        socket_.async_connect(*resolveResult, yield[ec]);
        if (ec) {
            using namespace boost::asio::error;
            switch (ec.value())
            {
            case connection_refused: handler(make_error_code(Errors::ConnectionRefused)); break;
            case host_unreachable: handler(make_error_code(Errors::HostUnreachable)); break;
            case already_connected: handler(make_error_code(Errors::AlreadyConnected)); break;
            default: handler(make_error_code(Errors::UnableToConnect)); break;
            }
            return;
        }
        handler(boost::system::error_code{});

        char buf[2048];
        while(true)
        {
            auto bytes = socket_.async_receive(boost::asio::buffer(buf, sizeof(buf)), yield[ec]);
            if (ec)
            {
                socket_.close();
                onDisconnect();
                return;
            }

            std::string_view data_sv(buf, bytes);
            onData(data_sv);
        }
    });
}

bool BasicNetwork::SendData(const NetworkInterface::DataType &data)
{
    if (!socket_.is_open())
        return false;

    auto buf = std::make_unique<NetworkInterface::DataType>(data);
    return SendData(std::move(buf));
}

bool BasicNetwork::SendData(NetworkInterface::DataType &&data)
{
    if (!socket_.is_open())
        return false;

    auto buf = std::make_unique<NetworkInterface::DataType>(std::move(data));
    return SendData(std::move(buf));
}

bool BasicNetwork::SendData(std::unique_ptr<std::string> &&buf)
{
    auto asioBuf = boost::asio::buffer(buf->data(), buf->size());
    socket_.async_send(asioBuf, [this, buf = std::move(buf)](const boost::system::error_code& ec, size_t /*bytes*/) {
        if (ec)
        {
            socket_.close();
        }
    });
    return true;
}

}
