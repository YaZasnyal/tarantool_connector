#pragma once

#include <optional>
#include <variant>
#include <cstdint>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <future>

namespace tarantool_connector
{

/**
 * @brief The Config contains compile time type definitions
 */
struct Config
{
    // MessagePack types
    using mp_uint = uint64_t;
    using mp_int = int64_t;
    using mp_string = std::string;
    using mp_bin = std::string;
    using mp_bool = bool;
    using mp_float = float;
    using mp_double = double;
    
    // Containers
    template<class T>
    using mp_array = std::vector<T>;
    template <class T, class V>
    using mp_map = std::unordered_map<T, V>;
    template <class T, class V>
    using mp_set = std::unordered_set<T, V>;
};

}
