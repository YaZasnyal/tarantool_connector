#pragma once

#include <array>
#include <string>
#include <sstream>

#include "header.h"
#include "tarantool_connector/tc_error.h"

namespace tarantool_connector
{

namespace detail
{

template<class Method = void>
struct Codec {};

std::string EncodeSize(const std::string& data)
{
    std::stringstream sizeBytes;
    msgpack::pack(sizeBytes, mp_int(data.size()));
    return sizeBytes.str();
}

} //detail

} //tarantool_connector

#include "methods/auth.h"
#include "methods/call.h"
