#pragma once

#include <atomic>
#include <mutex>

#include "connection.h"
#include "tarantool_connector/tc_error.h"
#include "result_handler.h"

namespace tarantool_connector
{
namespace detail
{

class RequestManager
{
public:
    RequestManager(ConnectionSptr connection) :
        connection_(connection)
    {
        connection_->onData.connect(std::bind(&RequestManager::OnData, this, std::placeholders::_1));
        connection_->onDisconnect.connect(std::bind(&RequestManager::OnDisconnect, this));
    }

    void Connect(const std::string& host, uint16_t port,
                 std::function<void(const boost::system::error_code&)>&& completionHandler)
    {
        connection_->Connect(host, port, std::move(completionHandler));
    }

    void Auth(const std::string& login, const std::string& password, std::function<void(const boost::system::error_code&)>&& completionHandler);
    //     select(int, int, Arg, offset, limit)

    template<class Res, class Arg>
    void Call(const std::string& functionName, Arg&& arg,
              std::function<void(const boost::system::error_code&, Res)>&& completionHandler);

    void SendData(SeqNumType seqNum, ResultHandlerInterfaceUptr&& handler, std::string &&data);
    void OnData(std::string_view data);
    void OnDisconnect();
    void CancelHandlers(Errors error);
    void Stop();

private:
    ConnectionSptr connection_;
    bool stopped_ = false;
    std::atomic<SeqNumType> seqNum_ = ATOMIC_VAR_INIT(0);
    std::map<SeqNumType, ResultHandlerInterfaceUptr> handlers_;
    std::mutex handlersMutex_;

};

template<class Res, class Arg>
void RequestManager::Call(const std::string& functionName, Arg&& arg,
                          std::function<void(const boost::system::error_code&, Codec<MethodCall>::DecodeReturnType<Res>)>&& completionHandler)
{
    using ReturnType = Codec<MethodCall>::DecodeReturnType<Res>;
    auto handler = std::make_unique<ResultHandler<MethodCall, Res>>(std::move(completionHandler));
    SeqNumType reqId = seqNum_++;
    try {
        auto encodedData = Codec<MethodCall>::Encode(reqId, std::forward<decltype(arg)>(arg));
        SendData(reqId, std::move(handler), std::move(encodedData));
    } catch (boost::system::error_code e) {
        handler->Cancel(e); return;
    } catch (const std::exception& e) {
        handler->Cancel(Errors::UnhandledException); throw e;
    }
}

void RequestManager::Auth(const std::string& login, const std::string& password, std::function<void (const boost::system::error_code&)> &&completionHandler)
{
    auto salt = connection_->GetSalt();
    if (salt.empty())
    {
        completionHandler(make_error_code(Errors::NotConnected));
        return;
    }

    auto handler = std::make_unique<ResultHandler<MethodAuth, int>>([completionHandler](const boost::system::error_code& err, int)
    {
        completionHandler(err);
    });
    SeqNumType reqId = seqNum_++;
    try {
        auto encodedData = Codec<MethodAuth>::Encode(reqId, login, password, salt);
        SendData(reqId, std::move(handler), std::move(encodedData));
    } catch(boost::system::error_code e) {
        handler->Cancel(e); return;
    } catch(const std::exception& e) {
        handler->Cancel(Errors::UnhandledException); throw e;
    }
}

void RequestManager::SendData(SeqNumType seqNum, ResultHandlerInterfaceUptr&& handler, std::string&& data)
{
    std::unique_lock lock(handlersMutex_);
    if (stopped_)
    {
        handler->Cancel(Errors::ConnectorStopped);
        return;
    }

    if (connection_->SendData(std::move(data)))
    {
        handlers_.emplace(std::make_pair(seqNum, std::move(handler)));
    }
    else
    {
        handler->Cancel(Errors::NotConnected);
    }
}

void RequestManager::OnData(std::string_view data)
{
    int seqnum = 0;

    //Get sequence number

    ResultHandlerInterfaceUptr handler;
    {
        std::unique_lock lock(handlersMutex_);
        auto it = handlers_.find(seqnum);

        if (it != handlers_.end())
        {
            handler = std::move(it->second);
            handlers_.erase(it);
        }
    }

    if (handler)
        handler->Decode(data);
}

void RequestManager::OnDisconnect()
{
    CancelHandlers(Errors::LostConnection);
}

void RequestManager::CancelHandlers(Errors error)
{
    std::unique_lock lock(handlersMutex_);
    for (auto& [seqNum, handler] : handlers_)
    {
        handler->Cancel(error);
    }
    handlers_.clear();
}

void RequestManager::Stop()
{
    std::unique_lock lock(handlersMutex_);
    stopped_ = true;
    lock.unlock();
    CancelHandlers(Errors::ConnectorStopped);
}

} //detail
} //tarantool_connector
