#pragma once

#include <string>

#include "tarantool_connector/detail/data_codec.h"

namespace tarantool_connector
{
namespace detail
{

struct MethodCall {};

template<>
struct Codec<MethodCall>
{
    template<class Arg>
    static std::string Encode(SeqNumType seqNum, Arg&& arg)
    {
        throw make_error_code(Errors::UnhandledException);
        return "";
    }

    template<class T>
    using DecodeReturnType = T;

    template<class Res>
    static DecodeReturnType<Res> Decode(std::string_view data)
    {
        return DecodeReturnType<Res>();
    }
};

} //detail
} //tarantool_connector
