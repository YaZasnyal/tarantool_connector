#pragma once

#include <array>
#include <string>
#include <sstream>

#include <boost/uuid/detail/sha1.hpp>
#include <boost/archive/iterators/binary_from_base64.hpp>
#include <boost/archive/iterators/transform_width.hpp>
#include <boost/endian.hpp>
#include <boost/algorithm/string.hpp>

#include "tarantool_connector/utils/sha1.h"
#include "tarantool_connector/detail/data_codec.h"

namespace tarantool_connector
{
namespace detail
{

namespace
{

std::string ScramblePassword(const std::string& password, std::string salt)
{
    // Decode hash from base64 to binary
    using namespace boost::archive::iterators;
    using base64_text = transform_width<binary_from_base64<std::string::const_iterator>, 8, 6>;

    boost::algorithm::trim(salt); // trim trailing spaces
    static constexpr int SCRAMBLE_SIZE = 20;

    // PiQuer, Jun 11 2012, answer on Agus, "Base64 encode using boost throw exception",
    // Stack Overflow, Jan 06 2021, https://stackoverflow.com/questions/10521581.
    unsigned int paddChars = std::count(salt.begin(), salt.end(), '=');
    std::replace(salt.begin(),salt.end(), '=', 'A'); // replace '=' by base64 encoding of '\0'
    std::string decodedSalt(base64_text(salt.begin()), base64_text(salt.end())); // decode
    decodedSalt.erase(decodedSalt.end() - paddChars, decodedSalt.end());  // erase padding '\0' characters

    unsigned char hash1[SCRAMBLE_SIZE];
    unsigned char hash2[SCRAMBLE_SIZE];
    unsigned char out[SCRAMBLE_SIZE];
    utils::SHA1_CTX ctx;

    utils::SHA1Init(&ctx);
    utils::SHA1Update(&ctx, reinterpret_cast<const unsigned char*>(password.data()), password.size());
    utils::SHA1Final(hash1, &ctx);

    utils::SHA1Init(&ctx);
    utils::SHA1Update(&ctx, hash1, SCRAMBLE_SIZE);
    utils::SHA1Final(hash2, &ctx);

    utils::SHA1Init(&ctx);
    utils::SHA1Update(&ctx, reinterpret_cast<const unsigned char*>(decodedSalt.data()), SCRAMBLE_SIZE);
    utils::SHA1Update(&ctx, hash2, SCRAMBLE_SIZE);
    utils::SHA1Final(out, &ctx);

    for (int i = 0; i < SCRAMBLE_SIZE; ++i)
    {
        out[i] = hash1[i] ^ out[i];
    }

    std::string scrambleStr;
    scrambleStr.resize(SCRAMBLE_SIZE);
    std::memcpy(const_cast<char*>(scrambleStr.data()), out, SCRAMBLE_SIZE);

    return scrambleStr;
}

}

struct MethodAuth {using reqID = std::integral_constant<mp_int, 0x07>;};

template<>
struct Codec<MethodAuth>
{
    static std::string Encode(SeqNumType seqNum, const std::string& login, const std::string& password, const std::string& salt)
    {
        if (login.empty())
            throw std::invalid_argument("login is empty");
        if (password.empty())
            throw std::invalid_argument("password is empty");
        if (salt.empty())
            throw std::invalid_argument("salt is empty");

        auto scramble = ScramblePassword(password, salt);

        // Format request
        std::stringstream buffer;
        msgpack::packer packer(buffer);

        Header header;
        header.seqNum = seqNum;
        header.method = MethodAuth::reqID::value;
        packer.pack(header);

        packer.pack_map(2);
        packer.pack(mp_int(0x23));
        packer.pack(login);
        packer.pack(mp_int(0x21));
        packer.pack(std::array<std::string, 2>{"chap-sha1", scramble});

        std::string bytes = buffer.str();

        return bytes;
    }

    template<class T>
    using DecodeReturnType = T;

    template<class Res>
    static DecodeReturnType<Res> Decode(std::string_view data)
    {
        return DecodeReturnType<Res>();
    }
};

} //detail
} //tarantool_connector
