#pragma once

#include <stdexcept>
#include <mutex>
#include <boost/asio/streambuf.hpp>

#include "tarantool_connector/detail/data_codec.h"
#include "tarantool_connector/network_interface.h"

namespace tarantool_connector
{
namespace detail
{

namespace  {
    static constexpr int msgLengthSize = 5;
}

class Connection
{
public:
    Connection(NetworkInterfaceSptr network) :
        network_(network)
    {
        if (!network_)
        {
            throw std::invalid_argument("Network must be a valid object but nullptr was passed");
        }
        network_->onData.connect(std::bind(&Connection::OnData, this, std::placeholders::_1));
        network_->onDisconnect.connect(std::bind(&Connection::OnDisconnect, this));
    }

    void Connect(const std::string &host, uint16_t port,
                 std::function<void(const boost::system::error_code&)>&& completionHandler);
    bool SendData(const NetworkInterface::DataType& data);
    bool SendData(NetworkInterface::DataType&& data);
    void OnData(std::string_view data);
    void OnDisconnect();

    /**
     * @brief GetSalt - returns salt for password hashing
     */
    std::string GetSalt();

    boost::signals2::signal<void(std::string_view)> onData;
    boost::signals2::signal<void()> onDisconnect;

private:
    bool ReadHello(std::string_view data);
    mp_int GetMsgSize();
    mp_int GetMsgSize(std::string_view sv);

    NetworkInterfaceSptr network_;
    std::function<void(const boost::system::error_code&)> pendingConnectRequest;
    bool connected_ = false;
    boost::asio::streambuf buffer;
    std::optional<mp_int> nextMsgLength = 0;
    std::mutex bufferMutex_;
    std::string salt_;
};
using ConnectionSptr = std::shared_ptr<Connection>;


void Connection::Connect(const std::string& host, uint16_t port,
                         std::function<void (const boost::system::error_code &)>&& completionHandler)
{
    std::unique_lock lock(bufferMutex_);
    if (pendingConnectRequest)
    {
        completionHandler(make_error_code(Errors::ConnectionInProgress));
        return;
    }
    auto handler = [this](const boost::system::error_code & ec) mutable {
        std::unique_lock lock(bufferMutex_);
        if (pendingConnectRequest && ec)
        {
            pendingConnectRequest(ec);
            pendingConnectRequest = decltype(pendingConnectRequest)();
        }
    };
    pendingConnectRequest = std::move(completionHandler);
    network_->ConnectAsync(host, port, handler);
}

bool Connection::SendData(const NetworkInterface::DataType &data)
{
    bool result = false;

    result = network_->SendData(EncodeSize(data));
    result = result && network_->SendData(data);
    return result;
}

bool Connection::SendData(NetworkInterface::DataType &&data)
{
    bool result = false;
    result = network_->SendData(EncodeSize(data));
    result = result && network_->SendData(std::move(data));
    return result;
}

void Connection::OnData(std::string_view data)
{
    std::unique_lock lock(bufferMutex_);
    if (!connected_)
    {
        if (!ReadHello(data)) return; // waiting for more data
        connected_ = true;
        if (pendingConnectRequest)
        {
            pendingConnectRequest(boost::system::error_code{});
            pendingConnectRequest = decltype(pendingConnectRequest)();
        }
        if (buffer.size())
        {
            // process data remaining in the buffer.
            OnData("");
        }
        return;
    }

    if (buffer.size())
    {
        std::ostream str(&buffer);
        str << data;

        while (auto size = GetMsgSize())
        {
            if (buffer.size() >= size)
            {
                std::string msg;
                msg.resize(size);
                std::istream istr(&buffer);
                istr.read(msg.data(), size);

                std::string_view msg_sv(msg.data(), msg.size());
                onData(msg_sv);
                nextMsgLength = std::nullopt;
            }
        }
    }
    else // if message is full there is no need to copy it to the buffer and then back
    {
        std::string_view unprocessedData(data.data(), data.size());
        while (unprocessedData.size())
        {
            mp_int msgSize = GetMsgSize(unprocessedData);
            if (msgSize > (unprocessedData.size() + msgLengthSize))
                break;

            auto msg = unprocessedData.substr(msgLengthSize, msgSize);
            onData(msg);
            unprocessedData = unprocessedData.substr(msgLengthSize + msgSize);
        }
        if (unprocessedData.size())
        {
            std::ostream str(&buffer);
            str << data;
        }
    }
}

void Connection::OnDisconnect()
{
    std::unique_lock lock(bufferMutex_);
    connected_ = false;
    buffer.consume(buffer.size());
    if (pendingConnectRequest)
    {
        pendingConnectRequest(make_error_code(Errors::LostConnection));
        pendingConnectRequest = decltype(pendingConnectRequest)();
    }
    onDisconnect();
}

std::string Connection::GetSalt()
{
    std::unique_lock lock(bufferMutex_);
    return salt_;
}

bool Connection::ReadHello(std::string_view data)
{
    constexpr size_t helloMessageSize = 128;
    std::ostream str(&buffer);
    str << data;

    if (buffer.size() >= helloMessageSize)
    {
        buffer.consume(64); // consume first line
        salt_.resize(64);
        std::istream istr(&buffer);
        istr.read(salt_.data(), 64);
        return true;
    }

    return false;
}

mp_int Connection::GetMsgSize()
{
    if (nextMsgLength)
        return nextMsgLength.value();

    if (buffer.size() <= msgLengthSize)
        return 0;

    std::istream istr(&buffer);
    char lenbuf[msgLengthSize];
    istr.read(lenbuf, msgLengthSize);
    auto size_sv = std::string_view(lenbuf, msgLengthSize);
    if (auto size = GetMsgSize(size_sv))
    {
        nextMsgLength = size;
        return size;
    }

    return 0;
}

mp_int Connection::GetMsgSize(std::string_view sv)
{
    if (sv.size() >= msgLengthSize)
    {
        return msgpack::unpack(sv.data(), sv.size())->as<mp_int>();
    }
    return 0;
}

} //detail
} //tarantool_connector
