#pragma once

#include <memory>
#include "tarantool_connector/tc_error.h"
#include "data_codec.h"

namespace tarantool_connector
{
namespace detail
{

class ResultHandlerInterface
{
public:
    virtual ~ResultHandlerInterface() = default;
    virtual void Decode(std::string_view data) = 0;
    virtual void Cancel(Errors error) = 0;
    virtual void Cancel(boost::system::error_code error) = 0;
};
using ResultHandlerInterfaceUptr = std::unique_ptr<ResultHandlerInterface>;

template<class Method, class Res>
class ResultHandler : public ResultHandlerInterface
{
public:
    using ResultType = typename Codec<Method>::template DecodeReturnType<Res>;

    ResultHandler(std::function<void(const boost::system::error_code&, ResultType)>&& completionHandler) :
        completionHandler_(std::forward<decltype (completionHandler)>(completionHandler))
    {}

    // ResultHandlerInterface interface
    void Decode(std::string_view data)
    {
        try
        {
            auto result = Codec<Method>::template Decode<Res>(data);
            completionHandler_(boost::system::error_code{}, result);
        }
        catch (boost::system::error_code e)
        {
            completionHandler_(e, Res());
        }
    }

    void Cancel(Errors error)
    {
        completionHandler_(make_error_code(error), Res());
    }

    void Cancel(boost::system::error_code error)
    {
        completionHandler_(error, Res());
    }

private:
    std::function<void(const boost::system::error_code&, ResultType)> completionHandler_;
};

} //detail
} //tarantool_connector
