#pragma once

#include <optional>

#include "types.h"

namespace tarantool_connector
{
namespace detail
{

struct Header
{
    mp_int method = 0; // Method or response code indocator (0x00)
    SeqNumType seqNum = 0;
    std::optional<mp_int> schemaVersion = 0;
};

} // detail
} // tarantool_connector

namespace msgpack {
MSGPACK_API_VERSION_NAMESPACE(MSGPACK_DEFAULT_API_NS) {
namespace adaptor {

template <>
struct pack<tarantool_connector::detail::Header> {
    template <typename Stream>
    msgpack::packer<Stream>& operator()(msgpack::packer<Stream>& o, tarantool_connector::detail::Header const& v) const
    {
        o.pack_map(2);
        o.pack(tarantool_connector::mp_int(0x00));
        o.pack(v.method);
        o.pack(tarantool_connector::mp_int(0x01));
        o.pack(v.seqNum);
        return o;
    }
};

template <>
struct convert<tarantool_connector::detail::Header> {
    template <typename Stream>
    const msgpack::object& operator()(const msgpack::object& o, tarantool_connector::detail::Header& v) const
    {
        if (o.type != msgpack::type::MAP) throw msgpack::type_error();
        if (o.via.map.size != 3) throw msgpack::type_error();

        for (int i = 0; i < o.via.map.size; ++i)
        {
            switch(o.via.map.ptr[i].key.as<tarantool_connector::mp_int>())
            {
            case 0x00:
                v.method = o.via.map.ptr[i].val.as<tarantool_connector::mp_int>();
                break;
            case 0x01:
                v.seqNum = o.via.map.ptr[i].val.as<tarantool_connector::SeqNumType>();
                break;
            case 0x05:
                v.schemaVersion = o.via.map.ptr[i].val.as<tarantool_connector::mp_int>();
                break;
            }
        }
        return o;
    }
};
} // namespace adaptor
} // MSGPACK_API_VERSION_NAMESPACE(MSGPACK_DEFAULT_API_NS)
} // namespace msgpack
