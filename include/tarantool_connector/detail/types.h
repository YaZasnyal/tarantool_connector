#pragma once

#include <cstdint>
#include <msgpack.hpp>

namespace tarantool_connector
{

using mp_int = int32_t;

using SeqNumType = mp_int;

}
