#include <utility>
#include <type_traits>

#include <boost/pfr.hpp>

namespace struct_util
{

namespace detail {
// FWD
template <class T, typename SeqT, SeqT ... SeqVal>
auto structure_tie_impl(T& t, std::integer_sequence<SeqT, SeqVal...>);
}

/**
 * @brief structure_tie - maps aggregate members to tuple of references
 *
 * Internal aggregates are also converted to tuple of references recursively
 *
 * @code
 * struct Foo
 * {
 *   int i;
 *   struct Bar {
 *     int j;
 *     std::string k;
 *   } bar;
 * };
 *
 * Foo foo;
 * auto tuple = struct_util::structure_tie(foo);
 * static_assert (std::is_same_v<decltype(tuple),
 *                               std::tuple<int&, std::tuple<int&, std::string&>>>,
 *                "Types are not equal");
 * @endcode
 *
 * @param aggregate Structure to convert
 */
template <class T>
auto structure_tie(T& aggregate)
{
    auto sequence = std::make_index_sequence<boost::pfr::tuple_size_v<T>>{};
    return ::struct_util::detail::structure_tie_impl(aggregate, sequence);
}

namespace detail {

template <class T,
          typename = std::enable_if_t<
              !std::is_aggregate_v<std::decay_t<T>>>
          >
auto format_value(T&& v) -> decltype(v)
{
    return v;
}

template <class T,
          typename = std::enable_if_t<
              std::is_aggregate_v<std::decay_t<T>>>
          >
auto format_value(T& v)
{
    return structure_tie(v);
}

template <class T, typename SeqT, SeqT ... SeqVal>
auto structure_tie_impl(T& t, std::integer_sequence<SeqT, SeqVal...>)
{
    auto ref_tuple = boost::pfr::structure_tie(t);
    return std::tuple<decltype(format_value(std::get<SeqVal>(ref_tuple)))...>
            (format_value(std::get<SeqVal>(ref_tuple))...);
}

} // struct_util::detail

} // struct_util
