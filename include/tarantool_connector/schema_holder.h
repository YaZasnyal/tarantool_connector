#pragma once

#include <string>
#include <map>
#include <boost/multi_index_container.hpp>

namespace tarantool_connector
{

/**
 * @brief The SchemaHolder class holds current database schema
 */
class SchemaHolder
{
public:
    struct Space
    {
        int id;
        std::string name;

//        std::map<std::string
    };
    void UpdateSchema();

private:
//    boost::multi_index_container<
//        Space,
//        boost::multi_index::indexed_by<
//            boost::multi_index::ordered_unique<
//                boost::multi_index::member<Space, std::string, &Space::name>
//            >
//        >
//  > data_;

};

}
