#pragma once

#include <optional>
#include <boost/system/errc.hpp>

namespace tarantool_connector
{

enum class Errors
{
    NotConnected = 1,
    ConnectionInProgress,
    AlreadyConnected,
    HostResolveError,
    ConnectionRefused,
    HostUnreachable,
    UnableToConnect,
    LostConnection,
    DecodeError,
    ConnectorStopped,
    UnhandledException,
};


class ErrorCategory : public boost::system::error_category
{
public:
    // error_category interface
    const char *name() const noexcept;
    bool equivalent(const boost::system::error_code &code, int condition) const noexcept;
    std::string message(int ev) const;
};

const boost::system::error_category& tc_category();

boost::system::error_code make_error_code(Errors e);

boost::system::error_condition make_error_condition(Errors e);

}
