#pragma once

#include <msgpack.hpp>

#include "config.h"
#include "schema_holder.h"
#include "network_interface.h"

namespace tarantool_connector
{

template<class TConfig>
class TarantoolConnector;

/**
 * @brief The Box struct allows to perform operations on Tarantool spaces
 *
 * @warning This object is not thread safe.
 */
template<class Config>
struct Box
{
    using IntegerType = typename Config::mp_int;
    using StringType = typename Config::mp_string;
    using IdType = std::variant<IntegerType, StringType>;
    using OptionalIdType = std::optional<IdType>;

    //Misc
    /**
     * @brief GetSpaceId - determine id of spece by name
     * @param spaceName - space name
     * @return spece id or empty optional
     */
    IntegerType GetSpaceId(const IdType& space);
    /**
     * @brief GetIndexId - determine id of index by name
     * @param spaceId - space numeric id
     * @param indexName - index name
     * @return index id or empty optional
     */
    IntegerType GetIndexId(int spaceId, const std::string& indexName);
    /**
     * @brief GetIndexId - determine id of index by name
     * @param spaceName - space name
     * @param indexName - index name
     * @return index id or empty optional
     */
    IntegerType GetIndexId(const std::string& spaceName, const std::string& indexName);

    // Box& Space(const IdType& space)
    // Box& Index(const IdType& index)

    //template<class Res>
    //ReturnType<std::vector<Res>> Select(...) {}
    //...
private:
    Box(NetworkInterface* network, SchemaHolder* schemaHolder);

    OptionalIdType spaceId;
    OptionalIdType indexId;
    /**
     * @brief network connection to perform requests
     * Not owning pointer. Will cause SegFault if used after connector destruction
     */
    NetworkInterface* network_; // not owning holding.
    SchemaHolder* schemaHolder_;

    friend TarantoolConnector<Config>; // Only TarantoolConnector can instantiate this class
};

template<class Config>
Box<Config>::Box(NetworkInterface* network, SchemaHolder* schemaHolder) :
        network_(network),
        schemaHolder_(schemaHolder)
    {}

}
