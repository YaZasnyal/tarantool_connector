#pragma once

#include "config.h"
#include "schema_holder.h"
#include "box.h"
#include "detail/request_manager.h"
#include "detail/connection.h"
#include "basic_network.h"

#include <boost/asio.hpp>

namespace tarantool_connector
{

template<class TConfig = Config>
class TarantoolConnector
{
public:   
     TarantoolConnector(NetworkInterfaceSptr network)
         : connection_(std::make_shared<detail::Connection>(network)),
           requestManager_(connection_)
     {

     }

     TarantoolConnector(boost::asio::io_context& ctx)
         : connection_(std::make_shared<detail::Connection>(std::make_shared<BasicNetwork>(ctx))),
           requestManager_(connection_)
     {

     }

     template<class Token = boost::asio::use_future_t<>>
     auto ConnectAsync(const std::string& host, uint16_t port, Token&& token = Token())
     {
         using result_type = typename boost::asio::async_result<std::decay_t<Token>, void(boost::system::error_code)>;
         typename result_type::completion_handler_type handler(std::forward<decltype(token)>(token));
         result_type result(handler);
         requestManager_.Connect(host, port, std::move(handler));
         return result.get();
     }
     void Connect(const std::string& host, uint16_t port)
     {
         ConnectAsync(host, port).get();
     }

     template<class Token = boost::asio::use_future_t<>>
     auto AuthAsync(const std::string& username, const std::string& password, Token&& token = Token())
     {
         using result_type = typename boost::asio::async_result<std::decay_t<Token>, void(boost::system::error_code)>;
         typename result_type::completion_handler_type handler(std::forward<decltype(token)>(token));
         result_type result(handler);
         auto completionHandler = [handler = std::move(handler)](const boost::system::error_code& ec) mutable
         {
             handler(ec);
         };
         requestManager_.Auth(username, password, std::move(completionHandler));
         return result.get();
     }
     auto Auth(const std::string& username, const std::string& password)
     {
         return AuthAsync(username, password).get();
     }

     template<class Res = msgpack::object_handle, class Arg = std::tuple<>, class Token = boost::asio::use_future_t<>>
     auto CallAsync(const std::string& functionName, Arg&& arg = Arg(), Token&& token = Token())
     {
         using ReturnType = detail::Codec<detail::MethodCall>::DecodeReturnType<Res>;
         using result_type = typename boost::asio::async_result<std::decay_t<Token>, void(boost::system::error_code, ReturnType)>;
         typename result_type::completion_handler_type handler(std::forward<decltype(token)>(token));
         result_type result(handler);
         auto completionHandler = [handler = std::move(handler)](const boost::system::error_code& ec, ReturnType res) mutable
         {
             handler(ec, res);
         };
         requestManager_.Call<Res, Arg>(functionName, std::forward<decltype(arg)>(arg), std::move(completionHandler));
         return result.get();
     }
     template<class Res = msgpack::object_handle, class Arg = std::tuple<>>
     auto Call(const std::string& functionName, Arg&& arg = Arg())
     {
         return std::move(CallAsync<Res, Arg>(functionName, std::forward<Arg>(arg)).get());
     }

    /**
     * @brief Box - construct new Box object to perform operations on spaces
     * @return new Box
     */
    Box<TConfig> Box();

    /**
     * @brief Stop - stop connector cancelling all pending and future requests
     *
     * Continuation after calling this function is impossible
     */
    void Stop()
    {
        requestManager_.Stop();
    }

private:
    SchemaHolder schemaHolder_;
    detail::ConnectionSptr connection_;
    detail::RequestManager requestManager_;
};

template<class TConfig>
Box<TConfig> TarantoolConnector<TConfig>::Box()
{
    return ::tarantool_connector::Box<TConfig>(schemaHolder_, connection_.get());
}

}
