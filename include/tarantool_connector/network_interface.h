#pragma once

#include <memory>
#include <functional>
#include <boost/signals2.hpp>
#include "tc_error.h"

namespace tarantool_connector
{

/**
 * @brief The NetworkInterface class describes interface to
 */
class NetworkInterface
{
public:
    using DataType = std::string;
    virtual ~NetworkInterface() = default;

    /**
     * @brief ConnectAsync - Estanlish connection to the database
     * @param handler - completion handler function
     *
     * This fuction is allowed to return ConnectionInProgress error if it was alredy called.
     *
     * On success return empty error code. On error returns one of:
     * * ConnectionInProgress
     * * AlreadyConnected
     * * ConnectionRefused
     */
    virtual void ConnectAsync(const std::string& host, uint16_t port,
                              std::function<void(const boost::system::error_code&)>&& handler) = 0;

//    /**
//     * @brief Connected - returns true if connection was established
//     */
//    virtual bool Connected() const = 0;

    /**
     * @brief SendData - sends data to the database asynchronously
     * @param data - msgpack encoded binary data
     * @return returns true if data was queued for sending
     */
    virtual bool SendData(const DataType& data) = 0;

    /**
     * @brief SendData - sends data to the database asynchronously
     * @param data - msgpack encoded binary data
     * @return returns true if data was queued for sending
     */
    virtual bool SendData(DataType&& data) = 0;

    /**
     * @brief onDisconnect - signal that should be called by connection
     * if disconnect occured
     */
    boost::signals2::signal<void()> onDisconnect;

    /**
     * @brief onData - callback that is called on data being recieved
     * from the database
     */
    boost::signals2::signal<void(std::string_view)> onData;
};
using NetworkInterfaceSptr = std::shared_ptr<NetworkInterface>;

}
