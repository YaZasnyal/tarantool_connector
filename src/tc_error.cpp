#include "tarantool_connector/tc_error.h"

namespace tarantool_connector
{

const char *ErrorCategory::name() const noexcept
{
    return "tarantool_connector";
}

bool ErrorCategory::equivalent(const boost::system::error_code &code, int condition) const noexcept
{
    switch (condition)
    {
    case static_cast<int>(Errors::NotConnected):
        return code == boost::system::error_code(boost::system::errc::not_connected, boost::system::system_category());
    case static_cast<int>(Errors::ConnectionInProgress):
        return code == boost::system::error_code(boost::system::errc::connection_already_in_progress, boost::system::system_category());
    case static_cast<int>(Errors::AlreadyConnected):
        return code == boost::system::error_code(boost::system::errc::already_connected, boost::system::system_category());
    case static_cast<int>(Errors::HostResolveError):
        return code == boost::system::error_code(boost::system::errc::connection_refused, boost::system::system_category());
    case static_cast<int>(Errors::ConnectionRefused):
        return code == boost::system::error_code(boost::system::errc::connection_refused, boost::system::system_category());
    case static_cast<int>(Errors::LostConnection):
        return code == boost::system::error_code(boost::system::errc::broken_pipe, boost::system::system_category());
    case static_cast<int>(Errors::HostUnreachable):
        return code == boost::system::error_code(boost::system::errc::host_unreachable, boost::system::system_category());
    case static_cast<int>(Errors::DecodeError):
        return code == boost::system::error_code(boost::system::errc::protocol_error, boost::system::system_category());
    case static_cast<int>(Errors::ConnectorStopped):
        return code == boost::system::error_code(boost::system::errc::operation_canceled, boost::system::system_category());
    default:
        return false;
    }
}

std::string ErrorCategory::message(int ev) const
{
    switch (ev)
    {
    case static_cast<int>(Errors::NotConnected):
        return "Not connected to the database";
    case static_cast<int>(Errors::ConnectionInProgress):
        return "Connection to the database is already in progress";
    case static_cast<int>(Errors::AlreadyConnected):
        return "Already connected to the database";
    case static_cast<int>(Errors::HostResolveError):
        return "Unable to resolve host";
    case static_cast<int>(Errors::ConnectionRefused):
        return "Remote host refused connection";
    case static_cast<int>(Errors::HostUnreachable):
        return "No route to host";
    case static_cast<int>(Errors::UnableToConnect):
        return "Unable to establish connection";
    case static_cast<int>(Errors::LostConnection):
        return "Connection to the database was lost";
    case static_cast<int>(Errors::DecodeError):
        return "Result decode failed";
    case static_cast<int>(Errors::ConnectorStopped):
        return "Connector was stopped";
    }

    return "No description to the error";
}

const boost::system::error_category& tc_category()
{
    static ErrorCategory instance;
    return instance;
}

boost::system::error_code make_error_code(Errors e)
{
    return boost::system::error_code(static_cast<int>(e), tc_category());
}

boost::system::error_condition make_error_condition(Errors e)
{
    return boost::system::error_condition(static_cast<int>(e), tc_category());
}

}
