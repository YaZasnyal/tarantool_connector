#include <iostream>
#include <algorithm>
#include <future>

#include <tarantool_connector/struct_reflector.h>

#include <tarantool_connector/tarantool_connector.h>
#include <msgpack.hpp>

#include <boost/asio/spawn.hpp>
#include <boost/fiber/all.hpp>

struct tcVoid {};

int main()
{
    boost::asio::io_context ctx;
    std::thread tr([&ctx](){
        boost::asio::io_context::work w(ctx);
        ctx.run();
    });

    tarantool_connector::TarantoolConnector connector(ctx); // (on_separate_thread|external_context)
    connector.Connect("51.15.118.139", 3301); // Connect synchronously
    connector.Auth("artem", "Qwerty123");

    // Synchronous call
    try
    {
        int res = connector.Call<int>("MeaningOfLife", tcVoid());
        std::cout << "Synchronous call: Result: " << res << std::endl;
    }
    catch (const boost::system::system_error& e)
    {
        std::cout << "Exception in sync call: " << e.what() << std::endl;
    }

    // Using future
    try
    {
        std::future<int> resFuture = connector.CallAsync<int>("MeaningOfLife", tcVoid());
        int res = resFuture.get();
        std::cout << "Using future: Result: " << res << std::endl;
    }
    catch (const boost::system::system_error& e)
    {
        std::cout << "Exception in async call with future: " << e.what() << std::endl;
    }


    // using completion handler
    connector.CallAsync<int>("MeaningOfLife", tcVoid(), [](const boost::system::error_code& ec, int res) -> void {
        if (!ec)
            std::cout << "Using completion handler: Result: " << res << std::endl;
        else
            std::cout << "Error in async call with callback: " << ec.message() << std::endl;
    });

    // using coroutines
    boost::asio::spawn(ctx, [&connector](boost::asio::yield_context yield){
        boost::system::error_code ec;
        int res = connector.CallAsync<int>("MeaningOfLife", std::tuple<>(), yield[ec]);
        if (!ec)
            std::cout << "Result coro1: " << res << std::endl;
        else
            std::cout << "Error in coro1: " << ec.message() << std::endl;
    });
    boost::asio::spawn(ctx, [&connector](boost::asio::yield_context yield){
        try
        {
            int res = connector.CallAsync<int>("MeaningOfLife", tcVoid(), yield);
            std::cout << "Result coro2: " << res << std::endl;
        }
        catch (const boost::system::system_error& e)
        {
            std::cout << "Exception in coro2: " << e.what() << std::endl;
        }
    });

    ctx.stop();
    tr.join();
    std::cout << "Finished" << std::endl;
    return 0;
}
